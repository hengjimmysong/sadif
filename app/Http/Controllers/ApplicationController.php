<?php

namespace Sadif\Http\Controllers;

use Sadif\Models\Application;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = Application::all()->reverse();
        return view('admin.applications.index', ['applications' => $applications]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $application = new Application();
        $application->company_name = $request->company_name;
        $application->contact_person = $request->contact_person;
        $application->email = $request->email;
        $application->phone = $request->phone;
        $application->website = $request->website;
        $application->launch = $request->launch;
        $application->description = $request->description;
        $application->target_customer = $request->target_customer;
        $application->revenue = $request->revenue;
        $application->staff_info = $request->staff_info;
        $application->outside_investment = $request->outside_investment;
        $application->message = $request->message;
        $application->agree = $request->agree;
        error_log('Meow');
        error_log($request);
        error_log($request->hasfile('cv'));
        if ($request->hasfile('cv')) {
            error_log('This is some useful information.');
            $file = $request->file('cv');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time() . '.' . $extension;
            $file->move('storage/uploads/applications/', $filename);
            $application->cv = 'storage/uploads/applications/' . $filename;
            error_log($filename);
        }
        $application->save();

        return redirect('/')->with('message', 'Application successfully submitted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Sadif\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $application = Application::find($id);
        return view('admin.applications.show', ['application' => $application]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Sadif\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function edit(Application $application)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Sadif\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Application $application)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Sadif\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function destroy(Application $application)
    {
        //
    }
}
