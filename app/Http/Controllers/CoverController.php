<?php

namespace Sadif\Http\Controllers;

use Illuminate\Http\Request;

class CoverController extends Controller
{
    public function store(Request $request, $page)
    {
        if ($request->hasfile('image')) {
            $file = $request->file('image');
            $filename = $page . '.png';
            $file->move('images/covers/', $filename);
        }

        return redirect('admin/dashboard');
    }
}
