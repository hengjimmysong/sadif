<?php

namespace Sadif\Http\Controllers;

use Illuminate\Http\Request;
use Sadif\Models\Investee;

class InvesteeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $investees = Investee::all();
        return view('admin.investees.index', ['investees' => $investees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.investees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $investee = new Investee();
        $investee->name = $request->name;
        $investee->date = $request->date;
        $investee->summary = $request->summary;
        $investee->website = $request->website;
        $investee->video = $request->video;
        
        if ($request->hasfile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time() . '.' . $extension;
            $file->move('storage/uploads/investees/images/', $filename);
            $investee->image = 'storage/uploads/investees/images/' . $filename;
        }

        if ($request->hasfile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time() . '.' . $extension;
            $file->move('storage/uploads/investees/logo/', $filename);
            $investee->logo = 'storage/uploads/investees/logo/' . $filename;
        }

        $investee->save();

        return redirect('/admin/investees');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $investee = Investee::find($id);
        return view('admin.investees.edit', ['investee' => $investee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $investee = Investee::find($id);
        $investee->name = $request->name;
        $investee->date = $request->date;
        $investee->summary = $request->summary;
        $investee->website = $request->website;
        $investee->video = $request->video;

        if ($request->hasfile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time() . '.' . $extension;
            $file->move('storage/uploads/investees/images/', $filename);
            $investee->image = 'storage/uploads/investees/images/' . $filename;
        }
        if ($request->hasfile('logo')) {
            $file = $request->file('logo');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time() . '.' . $extension;
            $file->move('storage/uploads/investees/logo/', $filename);
            $investee->image = 'storage/uploads/investees/logo/' . $filename;
        }
        $investee->save();

        return redirect('/admin/investees');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $investee = Investee::find($id);
        $investee->delete();
        return redirect('/admin/investees');
    }
}
