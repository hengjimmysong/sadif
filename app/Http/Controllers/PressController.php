<?php

namespace Sadif\Http\Controllers;

use Illuminate\Http\Request;
use Sadif\Models\Press;


class PressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $presses = Press::all();
        return view('admin.presses.index', ['presses' => $presses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.presses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $press = new Press();
        $press->title = $request->title;
        $press->date = $request->date;
        $press->content = $request->content;
        $press->description = $request->description;
        $press->user_id = \Auth::user()->id;
        if ($request->hasfile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time() . '.' . $extension;
            $file->move('storage/uploads/presses/', $filename);
            $press->image = 'storage/uploads/presses/' . $filename;
        }
        $press->save();

        return redirect('admin/presses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $press = Press::find($id);
        return view('admin.presses.edit', ['press' => $press]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $press = Press::find($id);
        $press->title = $request->title;
        $press->date = $request->date;
        $press->content = $request->content;
        $press->description = $request->description;
        $press->user_id = \Auth::user()->id;
        if ($request->hasfile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension(); // getting image extension
            $filename = time() . '.' . $extension;
            $file->move('storage/uploads/presses/', $filename);
            $press->image = 'storage/uploads/presses/' . $filename;
        }
        $press->save();

        return redirect('admin/presses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $press = Press::find($id);
        $press->delete();
        return redirect('/admin/presses');
    }
}
