<?php

namespace Sadif\Models;

use Illuminate\Database\Eloquent\Model;

class Investee extends Model
{
    protected $fillable = ['name', 'date', 'website', 'summary'];
}
