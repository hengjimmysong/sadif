<?php

namespace Sadif\Models;

use Illuminate\Database\Eloquent\Model;

class Press extends Model
{
    protected $fillable = ['title', 'date', 'description', 'content'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
