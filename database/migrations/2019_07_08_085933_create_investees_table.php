<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvesteesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('summary')->nullable();
            $table->string('date')->nullable();
            $table->string('website')->nullable();
            $table->string('image')->nullable();
            $table->string('logo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investees');
    }
}
