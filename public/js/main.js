function openMenu() {
  console.log('meow')
  let menu = document.getElementById('mobile-menu')
  let menuBtn = document.getElementById('menu-open')
  let closeBtn = document.getElementById('menu-close')
  menuBtn.classList.add('animate-out')
  menu.classList.remove('hide')
  menu.classList.add('unhide')

  setTimeout(() => {
    menuBtn.classList.remove('animate-out')
    menuBtn.classList.remove('unhide')
    menuBtn.classList.add('hide')
    closeBtn.classList.remove('hide')
    closeBtn.classList.add('unhide')
    closeBtn.classList.add('animate-in')
    setTimeout(() => {
      closeBtn.classList.remove('animate-in')
    }, 450);
  }, 450);
}

function closeMenu() {
  let menu = document.getElementById('mobile-menu')
  let menuBtn = document.getElementById('menu-open')
  let closeBtn = document.getElementById('menu-close')
  closeBtn.classList.add('animate-out')
  menu.classList.remove('unhide')
  menu.classList.add('hide')

  setTimeout(() => {
    closeBtn.classList.remove('animate-out')
    closeBtn.classList.remove('unhide')
    closeBtn.classList.add('hide')
    menuBtn.classList.remove('hide')
    menuBtn.classList.add('unhide')
    menuBtn.classList.add('animate-in')
    setTimeout(() => {
      menuBtn.classList.remove('animate-in')
    }, 450);
  }, 450);
}

function addRequire() {
  let form = document.getElementById('apply-form')
  form.classList.add('submitted')
}