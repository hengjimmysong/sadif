@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card" style="">
        <div class="card-header">
          <div class="row">
            <div class="col">Edit Press</div>
            <form action="{{url('/admin/presses', $press->id)}}" method="POST" onSubmit="return confirm('Are you sure you wish to delete?')">
              @method('DELETE')
              {{csrf_field()}}
              <input type="submit" class="btn btn-danger" value="Delete Press"/>
            </form>
          </div>
        </div>

        <form action="/admin/presses/{{$press->id}}" method="POST" enctype="multipart/form-data">
          <div class="col p-3">
            @method('PATCH')
            {{csrf_field()}}
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="image">Upload Image</span>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="image" name="image" aria-describedby="image">
                <label class="custom-file-label" for="image">Choose file</label>
              </div>
            </div>

            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" name="title" class="form-control" id="title" placeholder="Title"
                value="{{$press->title}}">
            </div>

            <div class="form-group">
              <label for="date">Date</label>
              <input type="date" name="date" class="form-control" id="date" placeholder="Date" value="{{$press->date}}">
            </div>

            <div class="form-group">
              <label for="description">Description</label>
              <textarea class="form-control" id="description" name="description" rows="3"
                placeholder="Description">{{$press->description}}</textarea>
            </div>

            <div class="form-group">
              <label for="description">Content</label>
              <textarea class="form-control" id="content" name="content" rows="8"
                placeholder="Content">{{$press->content}}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection