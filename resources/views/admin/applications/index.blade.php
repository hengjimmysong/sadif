@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card" style="">
        <div class="card-header">
          <div class="row">
            <div class="col">Applications</div>
          </div>
        </div>

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col">Date</th>
              <th scope="col">Company Name</th>
              <th scope="col">Contact Person</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($applications as $application)
            <tr>
              <th scope="row">{{$application->id}}</th>
              <th>{{ date('D, d M Y', strtotime($application->created_at)) }}</th>
              <td>{{$application->company_name}}</td>
              <td>{{$application->contact_person}}</td>
              <td><a href="/admin/applications/{{$application->id}}" class="btn btn-success">View Application</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection