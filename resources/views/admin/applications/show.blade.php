@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card" style="">
        <div class="card-header">
          <div class="row">
            <div class="col">Application Detail</div>
          </div>
        </div>

        <div class="application">
          <div class="row">
            <p class="col-2 label">Company Name:</p>
            <p class="col value">{{$application->company_name}}</p>
          </div>
          <div class="row">
            <p class="col-2 label">Company Name:</p>
            <p class="col value">{{$application->contact_person}}</p>
          </div>
          <div class="row">
            <p class="col-2 label">Email Address:</p>
            <p class="col value">{{$application->email}}</p>
          </div>
          <div class="row">
            <p class="col-2 label">Phone Number:</p>
            <p class="col value">{{$application->phone}}</p>
          </div>
          <div class="row">
            <p class="col-2 label">Website:</p>
            <p class="col value">{{$application->website}}</p>
          </div>
          <div class="row">
            <p class="col-2 label">CV:</p>
            <p class="col link value" onclick="window.open('{{URL::to('/').'/'.$application->cv}}')"')">Click here to see cv</p>
          </div>

          <p class="label">Have you launched your product?* </p>
          <p class="capitalize answer">{{$application->launch}}</p>

          <p class="question">Briefly describe your product or business model. If available, please include links
            to
            presentation documents here.*</p>
          <p class="answer">{{$application->description}}</p>

          <p class="question">Who are your target customers?*</p>
          <p class="answer">{{$application->target_customer}}</p>

          <p class="question">What is your monthly revenue for the past 12 months?* (ex.: October 2016: $10,000;
            September
            2016: $5,000)</p>
          <p class="answer">{{$application->revenue}}</p>

          <p class="question">List your co-founders, key staff, equity breakdown. Specify if staff is full-time,
            part-time or contractual.*</p>
          <p class="answer">{{$application->staff_info}}</p>

          <p class="question">Have you raised outside investment before? If yes, how much?*</p>
          <p class="answer">{{$application->outside_investment}}</p>

          <p class="question">Anything else you want to tell us?*</p>
          <p class="answer">{{$application->message}}</p>

            <p class="answer">I declare that by submitting this application form, I have read and agreed to the above
              terms
              and conditions, and that the information provided is true to the best of my knowledge.</p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection