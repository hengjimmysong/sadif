@extends('layouts.app')
@section('header')
@endsection

@section('content')
<div class="container dashboard">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div class="covers">
                        <div class="card">
                            <div class="image" style="background-image: url('{{asset('images/covers/Home.png')}}')">
                            </div>
                            <h4>Home</h4>
                            <form action="/admin/covers/Home" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="file" class="input" id="image" name="image">

                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                        </div>
                        <div class="card">
                            <div class="image" style="background-image: url('{{asset('images/covers/About.png')}}')">
                            </div>
                            <h4>About</h4>
                            <form action="/admin/covers/About" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="file" class="input" id="image" name="image">

                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                        </div>
                        <div class="card">
                            <div class="image"
                                style="background-image: url('{{asset('images/covers/Investee.png')}}')"></div>
                            <h4>Investee Portfolio</h4>
                            <form action="/admin/covers/Investee" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="file" class="input" id="image" name="image">

                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                        </div>
                        <div class="card">
                            <div class="image" style="background-image: url('{{asset('images/covers/Press.png')}}')">
                            </div>
                            <h4>Press</h4>
                            <form action="/admin/covers/Press" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="file" class="input" id="image" name="image">

                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                        </div>
                        <div class="card">
                            <div class="image" style="background-image: url('{{asset('images/covers/FAQ.png')}}')">
                            </div>
                            <h4>FAQ</h4>
                            <form action="/admin/covers/FAQ" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <input type="file" class="input" id="image" name="image">

                                <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                        </div>
                        <div class="card portfolio">
                                <div class="image" style="background-image: url('{{asset('images/covers/Portfolio.png')}}')">
                                </div>
                                <h4>Portfolios</h4>
                                <form action="/admin/covers/Portfolio" method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <input type="file" class="input" id="image" name="image">
    
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection