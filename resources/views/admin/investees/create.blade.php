@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card" style="">
        <div class="card-header">
          Create Investee Portfolio
        </div>

        <form action="/admin/investees" method="POST" enctype="multipart/form-data">
          <div class="col p-3">
            {{csrf_field()}}
            <div class="form-group input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="image">Upload Image</span>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="image" name="image" aria-describedby="image">
                <label class="custom-file-label" for="image">Choose file</label>
              </div>
            </div>

            <div class="form-group input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="logo">Upload Logo</span>
                </div>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="logo" name="logo" aria-describedby="image">
                  <label class="custom-file-label" for="logo">Choose file</label>
                </div>
              </div>

            <div class="form-group">
              <label for="name" required>Company Name</label>
              <input type="text" name="name" class="form-control" id="name" placeholder="Company Name">
            </div>

            <div class="form-group">
                <label for="title">Summary</label>
                <input type="text" name="summary" class="form-control" id="summary" placeholder="Summary">
              </div>

            <div class="form-group">
              <label for="date">Investment Date</label>
              <input type="date" name="date" class="form-control" id="date" placeholder="Investment Date"
                value="{{date("Y-m-d")}}">
            </div>

            <div class="form-group">
              <label for="website">Website Link</label>
              <input type="text" name="website" class="form-control" id="website" placeholder="Website">
            </div>

            <div class="form-group">
              <label for="video">Video Link</label>
              <input type="text" name="video" class="form-control" id="video" placeholder="Video Link">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection