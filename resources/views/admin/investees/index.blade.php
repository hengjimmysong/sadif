@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card" style="">
        <div class="card-header">
          <div class="row">
            <div class="col">Investee Portfolios</div>
            <a href="/admin/investees/create" class="btn btn-primary">Create Portfolio</a>
          </div>
        </div>

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col" style="width: 5rem">Image</th>
              <th scope="col" style="width: 5rem">Logo</th>
              <th scope="col">Company Name</th>
              <th scope="col">Website</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($investees as $investee)
            <tr>
              <th scope="row">{{$investee->id}}</th>
              <td style="width: 5rem">
                  <div class="image" style="background-image: url('{{URL::to('/').'/'.$investee->image}}')"></div>
                </td>
                <td style="width: 5rem">
                    <div class="image" style="background-image: url('{{URL::to('/').'/'.$investee->logo}}')"></div>
                  </td>
              <td>{{$investee->name}}</td>
              <td>{{$investee->website}}</td>
              <td><a href="/admin/investees/{{$investee->id}}/edit" class="btn btn-success">Edit Investee</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection