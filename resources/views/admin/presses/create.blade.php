@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card" style="">
        <div class="card-header">
          Create Press
        </div>

        <form action="/admin/presses" method="POST" enctype="multipart/form-data">
          <div class="col p-3">
            {{csrf_field()}}
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text" id="image">Upload Image</span>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" id="image" name="image" aria-describedby="image">
                <label class="custom-file-label" for="image">Choose file</label>
              </div>
            </div>

            <div class="form-group">
              <label for="title">Title</label>
              <input type="text" name="title" class="form-control" id="title" placeholder="Title" required>
            </div>

            <div class="form-group">
              <label for="date">Date</label>
              <input type="date" name="date" class="form-control" id="date" placeholder="Date"
                value="{{date("Y-m-d")}}">
            </div>

            <div class="form-group">
              <label for="description">Description</label>
              <textarea class="form-control" id="description" name="description" rows="3"
                placeholder="Description" required></textarea>
            </div>

            <div class="form-group">
              <label for="description">Content</label>
              <textarea class="form-control" id="content" name="content" rows="8" placeholder="Content" required></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'content' );
    CKEDITOR.replace( 'description' );
</script>
@endsection