@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col">
      <div class="card" style="">
        <div class="card-header">
          <div class="row">
            <div class="col">Presses</div>
            <a href="/admin/presses/create" class="btn btn-primary">Create Press</a>
          </div>
        </div>

        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Id</th>
              <th scope="col" style="width: 5rem">Image</th>
              <th scope="col">Title</th>
              <th scope="col">Date</th>
              <th scope="col">Description</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($presses as $press)
            <tr>
              <th scope="row">{{$press->id}}</th>
              <td style="width: 5rem">
                <div class="image" style="background-image: url('{{URL::to('/').'/'.$press->image}}')"></div>
              </td>
              <td>{{$press->title}}</td>
              <td>{{$press->date}}</td>
              <td>{{$press->description}}</td>
              <td><a href="/admin/presses/{{$press->id}}/edit" class="btn btn-success">Edit Press</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection