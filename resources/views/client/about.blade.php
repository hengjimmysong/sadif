@extends('layouts.main')
@section('header')
<title>About Us</title>
@endsection

@section('content')
<div class="about-us">
    <cover style="background-image: url('{{asset('images/covers/About.png')}}')"></cover>

    <section class="intro">
        <p class="title">About Us</p>
        <p class="description">The Smart Axiata Digital Innovation Fund (SADIF) is a venture capital fund created by
            Smart Axiata, and co-invested by Forte Insurance and Mekong Strategic Partners (MSP). SADIF invests in
            Cambodian-based digital and disruptive companies and startups involved in B2B/B2C businesses, intermediary
            services, and digital platforms. The fund is managed by MSP who has deep knowledge in investment and the
            technology sector with unparalleled networks in Cambodia.</p>

        <div class="col container">
            <div class="company smart" onclick="window.open('http://www.smart.com.kh')">
                <div class="image" style="background-image: url('{{asset('images/images/Smart.png')}}')"></div>
                <p class="title smart">Smart Axiata Co., Ltd.</p>
                <p class="description">Smart Axiata Co., Ltd., Cambodia’s leading mobile telecommunications operator,
                    currently serves 8 million subscribers under the 'Smart' brand. Smart is part of Axiata Group
                    Berhad, one of Asia’s largest telecommunications groups. The company continues to be at the
                    forefront of mobile technology advancement and is also rapidly transforming itself into a digital
                    lifestyle brand, having introduced many innovative offerings and lifestyle entertainment value
                    propositions, while playing a key role in developing the Kingdom’s digital startup ecosystem.</p>
            </div>

            <div class="company mekong" onclick="window.open('http://www.mekongstrategic.com')">
                <div class="image" style="background-image: url('{{asset('images/images/Mekong.png')}}')"></div>
                <p class="title mekong">Mekong Strategic Partners</p>
                <p class="description">Mekong Strategic Partners (MSP) is an investment management and advisory services
                    firm, specializing in the Greater Mekong Region with focus on high growth markets of the Mekong
                    region. Our team consists of professionals and entrepreneurs that have built, scaled, and invested
                    in numerous successful companies. We offer our portfolio companies a deep global knowledge in the
                    technology sectors and unparalleled networks in Cambodia.​​</p>
            </div>

            <div class="company forte" onclick="window.open('https://www.forteinsurance.com')">
                <div class="image" style="background-image: url('{{asset('images/images/Forte.png')}}')"></div>
                <p class="title forte">Forte Insurance</p>
                <p class="description">Established in 1999, Forte Insurance is the kingdom’s largest and leading
                    insurance company for the past 18 years, providing the most comprehensive range of commercial and
                    private insurance services in Cambodia. With a dedicated team of insurance professionals, Forte
                    today commands 46.8% market share and is rated A+ for Financial Strength and continues to achieve
                    double-digit percentage growth.</p>
            </div>
        </div>
    </section>

    <section class="team">
        <p class="title">The Team</p>
        <p class="description">The Fund is governed by an Investment Committee, consisting of representatives from Smart
            Axiata, Mekong Strategic Partners, Forte and two independent members.</p>

        <p class="subtitle">Investment Committee</p>

        <div class="container">
            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/Thomas.jpg')}}')"></div>
                <p class="name">Thomas Hundt</p>
                <p class="position">Chief Executive Officer</p>
                <p class="organization">Smart Axiata Co., Ltd.</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/Feiruz.jpg')}}')"></div>
                <p class="name">Feiruz Ikhwan</p>
                <p class="position">Chief Financial Officer</p>
                <p class="organization">Smart Axiata Co., Ltd.</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/Mervyn.jpg')}}')"></div>
                <p class="name">MERVYN CHEO</p>
                <p class="position">Director</p>
                <p class="organization">Forte Investment Holdings</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/David.jpg')}}')"></div>
                <p class="name">DAVID SOK DARA MARSHALL</p>
                <p class="position">Group CFO</p>
                <p class="organization">ISI Group</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/Zoe.jpg')}}')"></div>
                <p class="name">ZOË NG</p>
                <p class="position">Managing Director</p>
                <p class="organization">Raintree Development</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/John.jpg')}}')"></div>
                <p class="name">JOHN MCGINLEY</p>
                <p class="position">Managing Partner</p>
                <p class="organization">Mekong Strategic Partners</p>
            </div>
        </div>

        <p class="subtitle fund">Investment Team</p>

        <div class="container">
            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/Stephen.jpg')}}')"></div>
                <p class="name">STEPHEN HIGGINS</p>
                <p class="position">Managing Partner</p>
                <p class="organization">Mekong Strategic Partners</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/Bora.jpg')}}')"></div>
                <p class="name">BORA KEM</p>
                <p class="position">Partner</p>
                <p class="organization">Mekong Strategic Partners</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/BunMeng.jpg')}}')"></div>
                <p class="name">Bun Meng Leng</p>
                <p class="position">Senior Investment Analyst</p>
                <p class="organization">Mekong Strategic Partners</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/Jolyda.jpg')}}')"></div>
                <p class="name">Jolyda Sou</p>
                <p class="position">Investment Analyst</p>
                <p class="organization">Mekong Strategic Partners</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/MarkS.jpg')}}')"></div>
                <p class="name">Mark Selby</p>
                <p class="position">Senior Investment Analyst</p>
                <p class="organization">Mekong Strategic Partners</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/Nick.jpg')}}')"></div>
                <p class="name">Nick Boerema</p>
                <p class="position">Investment Manager</p>
                <p class="organization">Mekong Strategic Partners</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/Bophana.jpg')}}')"></div>
                <p class="name">Bophana Chap</p>
                <p class="position">Finance Manager</p>
                <p class="organization">Mekong Strategic Partners</p>
            </div>

            <div class="profile">
                <div class="image" style="background-image: url('{{asset('images/members/MarkI.jpg')}}')"></div>
                <p class="name">Mark Ilott</p>
                <p class="position">Entrepreneur-in-residence</p>
                <p class="organization">Mekong Strategic Partners</p>
            </div>
        </div>
    </section>

    @include('client/components/contact-form')
</div>
@endsection