@extends('layouts.main')
@section('header')
<title>Apply Now</title>
@endsection

@section('content')
<div class="apply-now">
  <cover style="background-image: url('{{asset('images/covers/FAQ.png')}}')"></cover>
  <section class="col apply">
    <p class="title">Please complete the form below to kick start the process.</p>
    <p class="description">Questions marked * are essential information needed for this application.</p>

    <form id="apply-form" action="/applications" method="POST" class="col" enctype="multipart/form-data">
      {{csrf_field()}}
      <input type="text" name="company_name" class="input" placeholder="Company Name*" required>
      <input type="text" name="contact_person" class="input" placeholder="Contact Person*" required>
      <input type="email" name="email" class="input" placeholder="Email*" required>
      <input type="text" name="phone" class="input" placeholder="Phone Number*" required>
      <input type="text" name="website" class="input" placeholder="Company Website (if available)" required>
      <input type="file" name="cv" class="input" placeholder="Attach your CV*">

      <p class="text">Have you launched your product?*</p>
      <div class="row input radio">
        <input type="radio" name="launch" id="launch_yes" value="yes" class="radio" required>
        <label for="launch_yes">Yes</label>
        <input type="radio" name="launch" id="launch_no" value="no" class="radio" required>
        <label for="launch_no">No</label>
      </div>

      <label for="description">Briefly describe your product or business model. If available, please include links to
        presentation documents here.*</label>
      <textarea name="description" class="input text" id="description" rows="3" required></textarea>

      <label for="target_customer">Who are your target customers?*</label>
      <textarea name="target_customer" class="input text" rows="3" required></textarea>

      <label for="revenue">What is your monthly revenue for the past 12 months?* (ex.: October 2016: $10,000; September
        2016: $5,000)</label>
      <textarea name="revenue" class="input text" rows="3" required></textarea>

      <label for="staff_info">List your co-founders, key staff, equity breakdown. Specify if staff is full-time,
        part-time or contractual.*</label>
      <textarea name="staff_info" class="input text" rows="3" required></textarea>

      <label for="outside_investment">Have you raised outside investment before? If yes, how much?*</label>
      <textarea name="outside_investment" class="input text" rows="3" required></textarea>

      <label for="message">Anything else you want to tell us?*</label>
      <textarea name="message" class="input text" rows="3" required></textarea>

      <div class="row agree">
        <input type="checkbox" id="agree" name="agree" class="checkbox" value="1" required>
        <label for="agree">I declare that by submitting this application form, I have read and agreed to the above terms
          and conditions, and that the information provided is true to the best of my knowledge.</label>
      </div>

      <input type="submit" value="Submit" class="btn submit" onclick="addRequire()">
    </form>
  </section>
  @include('client/components/contact-form')
</div>
@endsection