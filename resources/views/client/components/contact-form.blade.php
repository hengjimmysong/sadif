<div id="contact-us" class="col contact-form">
  <div class="col left">
    <p class="title">Contact Us</p>

    <p class="subtitle">For more information</p>
    <div class="row">
      <div class="icon" style="background-image: url('{{asset('images/icons/Call.png')}}')"></div>
      <p class="text">+855 10 646 891</p>
    </div>

    <div class="row">
      <div class="icon" style="background-image: url('{{asset('images/icons/Mail.png')}}')"></div>
      <p class="text">bk@mekongstrategic.com</p>
    </div>
    {{-- <div class="row language">
      <p class="subtitle">Languages: </p>
      <p class="text">English</p>
    </div> --}}

    <div class="row images" style="background-image: url('{{asset('images/images/Partner_Footer.png')}}')"></div>
  </div>
  <div class="col right">
    <p class="title">Get in touch</p>

    <form action="/contacts" method="POST">
      {{csrf_field()}}
      <input type="text" name="name" class="input" placeholder="Name" required>
      <input type="text" name="contact" class="input" placeholder="Email or Phone Number" required>
      <textarea name="message" class="input text" placeholder="Message" rows="3" required></textarea>

      <input type="submit" value="Submit" class="btn submit">
    </form>
  </div>
</div>