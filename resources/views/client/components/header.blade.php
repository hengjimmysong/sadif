<div class="header">
  <div class="btn image" style="background-image: url('{{asset('images/images/Sadif.png')}}')"
    onclick="window.location = '/'"></div>
  <div class="btn-ctn">
    <div id="menu-open" class="btn menu-btn open" style="background-image: url('{{asset('images/icons/Menu.png')}}')"
      onclick="openMenu()"></div>
    <div id="menu-close" class="btn menu-btn close hide"
      style="background-image: url('{{asset('images/icons/Cross.png')}}')" onclick="closeMenu()">
    </div>
  </div>
  <div class="row web-menu">
    <div class="btn item {{Route::current()->getName() == 'about' ? 'active' : ''}}" onclick="window.location = '/about-us'">
    About Us</div>
    <div class="btn item {{Route::current()->getName() == 'portfolio' ? 'active' : ''}}" onclick="window.location = '/investment-portfolio'">Investment Portfolio</div>
    <div class="btn item {{Route::current()->getName() == 'press' ? 'active' : ''}}" onclick="window.location = '/press'">Press</div>
    <div class="btn item {{Route::current()->getName() == 'faq' ? 'active' : ''}}" onclick="window.location = '/faq'">FAQ</div>
    <div class="btn item" onclick="window.location = '#contact-us'">Contact Us</div>
    <div class="spacer"></div>
    <div class="btn apply-now" onclick="window.location = '/apply-now'">Apply Now</div>
  </div>
  <div id="mobile-menu" class="mobile-menu hide">
    <div class="btn item" onclick="window.location = '/about-us'">About Us</div>
    <div class="btn item" onclick="window.location = '/investment-portfolio'">Investment Portfolio</div>
    <div class="btn item" onclick="window.location = '/press'">Press</div>
    <div class="btn item" onclick="window.location = '/faq'">FAQ</div>
    <div class="btn item" onclick="window.location = '#contact-us'">Contact Us</div>
    <div class="btn item apply-now" onclick="window.location = '/apply-now'">Apply Now</div>
  </div>
</div>