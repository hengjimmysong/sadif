@extends('layouts.main')
@section('header')
<title>FAQ</title>
@endsection

@section('content')
<div class="faq">
  <cover style="background-image: url('{{asset('images/covers/FAQ.png')}}')"></cover>

  <section class="faq">
    <p class="title">FAQ</p>

    <p class="question">How big is the fund?</p>
    <p class="answer">Our first fund has the ability to invest up to 5 million USD.</p>

    <p class="question">What is the typical size of an investment?</p>
    <p class="answer">Individual investment will vary according to the stage, industry and funding requirement by the
      entrepreneurial team. A typical investment will range from 100,000 USD to 500,000 USD. In exceptional cases, these may
      vary.</p>

    <p class="question">What is Mekong Strategic Partners’ (MSP) role?</p>
    <p class="answer">MSP is the investment manager for the fund and will guide the investment process from beginning
      till end. MSP is responsible for generating deal flows, evaluating companies, performing due diligence, draft term
      sheets and presenting to the investment committee. The MSP team will also monitor and assist the portfolio
      company. In some cases, MSP will advise on an exit strategy.</p>

    <p class="question">How long will the fund be open for investments?</p>
    <p class="answer">We are committed to finding quality start-ups and teams, however long it may take. Typically, it
      takes about three years to fully invest and the portfolio will have at least 10 investments.</p>

    <p class="question">What type of start-ups can apply for funding?</p>
    <p class="answer">We look for start-ups that are in the digital space. They may include companies in the following
      areas: Payments, Security, Mobile, Social Media, E-commerce, Marketplace, Marketing and advertising, Gaming,
      Cloud, Enterprise software, Health, Internet of Things. The start up teams should have some, if not all, of its operations in Cambodia. We are not able to invest in
      hardware companies.</p>

    <p class="question">What type of team are you looking for?</p>
    <p class="answer">There is no typical mold that will ensure the success of a company. Alternatively, successful
      companies come in all types and sizes. However, the start-ups will be evaluated on a combination of different
      factors: the merit and feasibility of their business model, market attractiveness, technical competency,
      persistence, intelligence, problem solving, passion and drive for success. We founded and ran companies in the
      past, so we understand how challenging it is. Don’t worry if you don’t have the “complete package”, no one does!
    </p>

    <p class="question">What stage of companies are you looking to invest?</p>
    <p class="answer">We will invest in early stage and growth stage companies. We don’t feel comfortable investing in
      ideas without a minimum viable product or good product market fit.</p>

    <p class="question">How do I submit my business plan / pitch?</p>
    <p class="answer">Please click <span class="link" onclick="window.location = '/apply-now'">here</span> or contact <span class="link" onclick="window.open('http://www.mekongstrategic.com/investments.html')">mekongstrategic</span></p>

    <p class="question">Is Smart Axiata Digital Innovation Fund interested in partnership?</p>
    <p class="answer">We believe in the power of collaboration to create a thriving technology and investment ecosystem.
      We look to collaborate with leading entrepreneurs, strategic corporations, government agencies, innovators and
      thought leaders. Please <span class="link" onclick="window.location = '#contact-us'">contact us</span>.</p>

    <p class="question">I am investor. Can I participate in the fund?</p>
    <p class="answer">Please <span class="link" onclick="window.location = '#contact-us'">contact us</span> here. There may be cases where we look to co-fund with appropriate capital
      partners.</p>
  </section>
  @include('client/components/contact-form')
</div>
@endsection