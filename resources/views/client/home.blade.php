@extends('layouts.main')
@section('header')
<title>Home</title>
@endsection

@section('content')
<div class="home">
    <section class="col cover">
        <div class="image" style="background-image: url('{{asset('images/covers/Home.png')}}')"></div>
        <div class="col content">
            <div class="col left">
                <p class="title">What is SADIF?</p>
                <p class="description">Smart Axiata Digital Innovation Fund (SADIF) is a 5 million USD venture capital
                    fund which aims to spur the digital ecosystem in Cambodia. SADIF invests in Cambodian-based, digital
                    companies and startups.</p>
                {{-- <div class="hide-web row btn-ctn">
                    <div class="btn row item" onclick="window.location = '#objective'">
                        <div class="icon" style="background-image: url('{{asset('images/icons/Scroll.png')}}')"></div>
                        <p class="label">Scroll Down</p>
                    </div>
                    <div class="btn row item" onclick="window.open('https://www.youtube.com/watch?v=Zgg1DCTQjSw')">
                        <div class="icon" style="background-image: url('{{asset('images/icons/Play.png')}}')"></div>
                        <p class="label">Watch Video</p>
                    </div>
                </div> --}}
            </div>
            <div class="right">
                <iframe class="video" src="https://www.youtube.com/embed/Zgg1DCTQjSw?controls=0" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope" allowfullscreen></iframe>
            </div>
        </div>
    </section>

    <section id="objective" class="hover our-objective">
        <p class="title">Our Objectives</p>
        <p class="description">SADIF invests in the next-generation, transformational companies in the digital ecosystem
            and stimulate their growth.</p>

        <div class="container">
            <div class="col item objective">
                <div class="row">
                    <div class="icon" class="right" style="background-image: url('{{asset('images/icons/Fund.png')}}')">
                    </div>
                    <p class="subtitle">Fund</p>
                </div>
                <div class="col description">
                    <p>
                        High-growth companies that will shape Cambodia's digital landscape
                    </p>
                </div>
            </div>

            <div class="col item objective">
                <div class="row">
                    <div class="icon" style="background-image: url('{{asset('images/icons/Generate.png')}}')"></div>
                    <p class="subtitle">Generate</p>
                </div>
                <div class="col description">
                    <p>
                        More valuable companies that enrich lives
                    </p>
                </div>
            </div>

            <div class="col item objective">
                <div class="row">
                    <div class="icon" style="background-image: url('{{asset('images/icons/Nuture.png')}}')"></div>
                    <p class="subtitle">Nurture</p>
                </div>
                <div class="col description">
                    <p>
                        Growth and innovation beyond expectation
                    </p>
                </div>
            </div>

            <div class="col item objective">
                <div class="row">
                    <div class="icon" style="background-image: url('{{asset('images/icons/Spur.png')}}')"></div>
                    <p class="subtitle">Spur</p>
                </div>
                <div class="col description">
                    <p>
                        Cambodia’s digital ecosystem
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="hover our-values">
        <p class="title">Our Values</p>
        <p class="description">SADIF is more than just a fund. It will provide unparalleled support for startups,
            leveraging Smart’s expertise, platforms, marketing, infrastructure and customer base.</p>

        <div class="container yellow">
            <div class="col item value">
                <div class="row">
                    <div class="icon" style="background-image: url('{{asset('images/icons/Capital.png')}}')"></div>
                    <p class="subtitle">Investment Capital</p>
                </div>
                <div class="col description">
                    <p>
                        First large scale investment capital to fund early-stage tech companies in Cambodia
                    </p>
                </div>
            </div>

            <div class="col item value">
                <div class="row">
                    <div class="icon" style="background-image: url('{{asset('images/icons/Marketing.png')}}')"></div>
                    <p class="subtitle">Marketing</p>
                </div>
                <div class="col description">
                    <p>
                        Leverage Smart’s best-in-class brand and wide-reaching marketing channels
                    </p>
                </div>
            </div>

            <div class="col item value">
                <div class="row">
                    <div class="icon" style="background-image: url('{{asset('images/icons/Mentoring.png')}}')"></div>
                    <p class="subtitle">Mentoring</p>
                </div>
                <div class="col description">
                    <p>
                        Catalyze growth with support from industry leaders and experienced investors
                    </p>
                </div>
            </div>

            <div class="col item value">
                <div class="row">
                    <div class="icon" style="background-image: url('{{asset('images/icons/Strategy.png')}}')"></div>
                    <p class="subtitle">Strategic Access</p>
                </div>
                <div class="col description">
                    <p>
                        Access to more than 8 million Smart subscribers, digital platforms, additional infrastructure, and
                        unique insights
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="col investment-portfolio">
        <p class="title">Investment Portfolio</p>
        <p class="description">SADIF focuses on early-stage to growth-stage tech companies that are driving the digital
            transformation in Cambodia.</p>

        <img src="{{asset('images/covers/Portfolio.png')}}" alt class="image">
    </section>

    <section class="application-process">
        <p class="title">Application Process</p>
        <p class="description">If you are an entrepreneur with the talent to build an exceptional company, please fill
            up our online application to begin the conversation with us.</p>
        {{-- 
        <div class="container">
            <div class="col process">
                <div class="number">1</div>
                <p class="subtitle">Investment Capital</p>
                <p class="description">
                    First large scale investment capital to fund early-stage tech companies in Cambodia
                </p>
            </div>

            <div class="col process">
                <div class="number">2</div>
                <p class="subtitle">Marketing</p>
                <p class="description">
                    Leverage Smart’s best-in-class brand and wide-reaching marketing channels
                </p>
            </div>

            <div class="col process">
                <div class="number">3</div>
                <p class="subtitle">Mentoring</p>
                <p class="description">
                    Catalyze growth with support from industry leaders and experienced investors
                </p>
            </div>

            <div class="col process">
                <div class="number">4</div>
                <p class="subtitle">Strategic Access</p>
                <p class="description">
                    Access to more than 8 million Smart subscribers, digital platforms, additional infrastructure, and
                    unique insights
                </p>
            </div>

            <div class="col process">
                <div class="number">5</div>
                <p class="subtitle">Strategic Access</p>
                <p class="description">
                    Access to more than 8 million Smart subscribers, digital platforms, additional infrastructure, and
                    unique insights
                </p>
            </div>
        </div> --}}
        <div class="btn apply-now" onclick="window.location = '/apply-now'">Apply Now</div>
    </section>

    @include('client/components/contact-form')
</div>
@endsection