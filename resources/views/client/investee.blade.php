@extends('layouts.main')
@section('header')
<title>Investee Portfolios</title>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
@endsection

@section('content')
<div class="investee">
    <cover style="background-image: url('{{asset('images/covers/Investee.png')}}')"></cover>

    <section class="investee-swiper">
        <p class="title">Investment Portfolio</p>

        <div class="swiper-container">
            <div class="swiper-wrapper">
                @foreach ($investees as $index=>$investee)
                <div class="swiper-slide">
                    <div class="ctn">
                        <div class="col card">
                            <div class="image" style="background-image: url('{{URL::to('/').'/'.$investee->image}}')">
                            </div>
                            <div class="col content">
                                <div class="logo" style="background-image: url('{{URL::to('/').'/'.$investee->logo}}')">
                                </div>
                                <p class="title">{{$investee->name}}</p>

                                <div class="row detail">
                                    <div class="icon"
                                        style="background-image: url('{{asset('images/icons/Suitcase.png')}}')"></div>
                                    <div class="info">
                                        <p class="text">{{$investee->summary}}</p>
                                    </div>
                                </div>

                                <div class="row detail">
                                    <div class="icon"
                                        style="background-image: url('{{asset('images/icons/Date.png')}}')"></div>
                                    <div class="info">
                                        <p class="text">Investment in
                                            {{ date('F Y', strtotime($investee->date)) }}
                                        </p>
                                    </div>
                                </div>

                                <div class="row detail website" onclick="window.open('http://{{$investee->website}}')">
                                    <div class="icon"
                                        style="background-image: url('{{asset('images/icons/Web.png')}}')"></div>
                                    <div class="info">
                                        <p class="text">{{$investee->website}}</p>
                                    </div>
                                </div>
                                @if ($investee->video)
                                <div class="btn label" onclick="window.open('http://{{$investee->video}}')"
                                    style="background-image: url('{{asset('images/icons/Play-Fill.png')}}')"></div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </section>

    <section class="investee-listing">
        <p class="title">Investment Portfolio</p>

        @foreach ($investees as $investee)
        <div class="col investee-item">
            <div class="logo" style="background-image: url('{{URL::to('/').'/'.$investee->logo}}')"></div>
            <div class="row">
                <div class="col left">
                    <p class="title">{{$investee->name}}</p>

                    <div class="row detail">
                        <div class="icon" style="background-image: url('{{asset('images/icons/Suitcase.png')}}')">
                        </div>
                        <div class="info">
                            <p class="text">{{$investee->summary}}</p>
                        </div>
                    </div>

                    <div class="row detail">
                        <div class="icon" style="background-image: url('{{asset('images/icons/Date.png')}}')"></div>
                        <div class="info">
                            <p class="text">Investment in {{ date('F Y', strtotime($investee->date)) }}</p>
                        </div>
                    </div>

                    <div class="row detail website" onclick="window.open('http://{{$investee->website}}')">
                        <div class="icon" style="background-image: url('{{asset('images/icons/Web.png')}}')"></div>
                        <div class="info">
                            <p class="text">{{$investee->website}}</p>
                        </div>
                    </div>

                    @if ($investee->video)
                    <button class="btn submit" onclick="window.open('http://{{$investee->video}}')">Watch Video</button>
                    @endif
                </div>
                <div class="right image" style="background-image: url('{{URL::to('/').'/'.$investee->image}}')"></div>
            </div>
        </div>
        @endforeach

    </section>

    @include('client/components/contact-form')
</div>
@endsection

@section('script')
<script>
    var mySwiper = new Swiper ('.swiper-container', {
              // Optional parameters
              direction: 'horizontal',
              loop: true,
          
              // If we need pagination
              pagination: {
                el: '.swiper-pagination',
              },

              autoplay: {
                delay: 3000,
              },
            })
</script>
@endsection