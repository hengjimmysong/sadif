@extends('layouts.main')
@section('header')
<title>Sadif - {{$press->title}}</title>
@endsection

@section('content')
<div class="press-detail">
  <cover style="background-image: url('{{URL::to('/').'/'.$press->image}}')"></cover>

  <section class="container">
    <p class="date">{{ date('F dS, Y', strtotime($press->date)) }}</p>
    <p class="title">{{$press->title}}</p>

    {{-- <div class="description">{!!$press->description!!}</div> --}}
    {{-- <div class="image" style="background-image: url('{{URL::to('/').'/'.$press->image}}')"></div> --}}

    <div class="rich content">{!!$press->content!!}</div>

  </section>

  @include('client/components/contact-form')
</div>
@endsection