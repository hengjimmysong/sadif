@extends('layouts.main')
@section('header')
<title>Press</title>
@endsection

@section('content')
<div class="press">
    <cover style="background-image: url('{{asset('images/covers/Press.png')}}')"></cover>

    <section class="press-listing">
        <p class="title">Press</p>

        @foreach ($presses as $press)

        <div class="row press-item">
            <div class="image" style="background-image: url('{{URL::to('/').'/'.$press->image}}')"></div>
            <div class="col info">
                <p class="date">{{ date('F dS, Y', strtotime($press->date)) }}</p>
                <p class="title">{{$press->title}}</p>
                <div class="description">
                    <p>{!!$press->description!!}</p>
                </div>

                <button class="btn submit" onclick="window.location = '/press/{{$press->id}}'">Read More</button>
            </div>
        </div>
        @endforeach
    </section>

    @include('client/components/contact-form')
</div>
@endsection