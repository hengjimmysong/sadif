<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  @yield('header')
  <link rel="icon" href="{{asset('images/icons/50px.png')}}">

  <!-- Sripts -->
  <script src="{{ asset('js/main.js') }}" defer></script>
  <!-- Styles -->
  <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>

<body>
  @if(session()->has('message'))
  <div class="alert">{{ session()->get('message') }}</div>
  @endif
  
  @include('client.components.header')
  @yield('content')
  @yield('script')
</body>

</html>