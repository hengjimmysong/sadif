<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Sadif\Models\Press;
use Sadif\Models\Investee;

Route::get('/', function () {
    $investees = Investee::all();
    return view('client.home', ['investees' => $investees]);
})->name('home');

Route::get('/about-us', function () {
    return view('client.about');
})->name('about');

Route::get('/faq', function () {
    return view('client.faq');
})->name('faq');

Route::get('/press', function () {
    $presses = Press::all()->sortBy('date')->reverse();
    return view('client.press', ['presses' => $presses]);
})->name('press');

Route::get('/press/{id}', function ($id) {
    $press = Press::find($id);
    return view('client.press-detail', ['press' => $press]);
})->name('press-detail');

Route::get('/investment-portfolio', function () {
    $investees = Investee::all();
    return view('client.investee', ['investees' => $investees]);
})->name('portfolio');

Route::get('/apply-now', function () {
    return view('client.apply');
});

// Route::get('/contact-us', function () {
//     return view('client.contact');
// });
Route::post('/contacts', 'ContactController@store');
Route::post('/applications', 'ApplicationController@store');


Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', function () {
        return redirect('/admin/dashboard');
    });
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    // Route::group(['prefix' => 'users'], function () {
    //     Route::get('/', 'UserController@index');
    //     Route::get('/create', 'UserController@create');
    // }); 
    Route::resources(['users' => 'UserController']);
    Route::resources(['presses' => 'PressController']);
    Route::resources(['investees' => 'InvesteeController']);
    Route::resources(['contacts' => 'ContactController']);
    Route::resources(['applications' => 'ApplicationController']);

    Route::post('/covers/{page}', 'CoverController@store');
});
